package com.zerda.s01activity;

public class S01Activity {

    public static void main(String[] args) {
        int legs = 4;
        char name = 'B';
        boolean isHangry = true;
        String pet = "Cat";

        System.out.println("My robot pet " + pet + " named " + name + " has " + legs + " legs. I asked it if it was hangry and it responded: " +isHangry);
    }
}
